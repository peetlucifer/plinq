﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HomeWork
{
	public class CompanyModel
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Department { get; set; }
		public int Age { get; set; }
		public int Salary { get; set; }
	}

	public class CompanyModelDTOForAVGSalary
	{
		public string Department { get; set; }
		public int Salary { get; set; }
	}

	public class CompanyModelDTOForMaxSalary
	{
		public AgeGroup AgeGroup { get; set; }
		public int Salary { get; set; }
	}

	public enum AgeGroup
	{
		Other = 0,
		Young = 1,
		Adult = 2,
		Old = 3
	}
}
