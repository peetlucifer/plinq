﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace HomeWork
{
	public class ParallelData
	{
		private List<CompanyModel> _listOfCompanyModel = new List<CompanyModel>();

		public void Reader()
		{
			using (var reader = new StreamReader(Directory.GetCurrentDirectory() + "/company.csv"))
			{
				reader.ReadLine();

				while (!reader.EndOfStream)
				{
					var line = reader.ReadLine();
					var values = line.Split(',');

					var model = new CompanyModel()
					{
						FirstName = values[0],
						LastName = values[1],
						Department = values[2],
						Age = Convert.ToInt32(values[3]),
						Salary = Convert.ToInt32(values[4])
					};
					_listOfCompanyModel.Add(model);
				}
			}
		}

		public List<CompanyModelDTOForAVGSalary> EvaluateAverageSalaryPerDepartment()
		{
			return _listOfCompanyModel
				.AsParallel()
				.GroupBy(d => d.Department)
				.Select(g => new CompanyModelDTOForAVGSalary
				{
					Department = g.Key,
					Salary = (int)g.Average(t => t.Salary)
				})
				.ToList();
		}

		public List<CompanyModelDTOForMaxSalary> FindMaxSalaryForAgeGroups()
		{
			return _listOfCompanyModel
				.AsParallel()
				.GroupBy(g => g.Age switch
				{
					{ } when g.Age >= 17 && g.Age < 30 => 1,
					{ } when g.Age >= 30 && g.Age < 46 => 2,
					{ } when g.Age >= 46 => 3,
					_ => 0
				})
				.OrderBy(o => o.Key)
				.Select(g => new CompanyModelDTOForMaxSalary
				{
					AgeGroup = (AgeGroup)g.Key,
					Salary = g.Max(t => t.Salary)
				})
				.ToList();
		}
	}
}
