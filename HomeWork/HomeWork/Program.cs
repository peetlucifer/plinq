﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HomeWork
{
	class Program
	{
		private static List<string> _listOfResult = new List<string>();

		static void Main(string[] args)
		{
			CallData();
			ParallelCallData();

			Console.WriteLine(_listOfResult[0] + "\n" + _listOfResult[1]);
		}
		public static void CallData()
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();

			Data data = new Data();
			data.Reader();

			foreach (var item in data.EvaluateAverageSalaryPerDepartment())
			{
				Console.WriteLine($"Department = '{item.Department}' and AVG Salary = {item.Salary}");
			}

			Console.WriteLine("\n");

			foreach (var item in data.FindMaxSalaryForAgeGroups())
			{
				if (item.AgeGroup == AgeGroup.Young)
					Console.WriteLine($"Age from 17 to 30 and MAX Salary = {item.Salary}");
				else if (item.AgeGroup == AgeGroup.Adult)
					Console.WriteLine($"Age from 30 to 45 and MAX Salary = {item.Salary}");
				else if (item.AgeGroup == AgeGroup.Old)
					Console.WriteLine($"Age from 46 and MAX Salary = {item.Salary}");
				else if(item.AgeGroup == AgeGroup.Other)
					Console.WriteLine($"Other age and MAX Salary = {item.Salary}");
			}

			Console.WriteLine("\n\n\n");

			stopwatch.Stop();
			_listOfResult.Add($"Summary time elapsed for parallel data: {stopwatch.Elapsed}");
		}

		public static void ParallelCallData()
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();

			ParallelData data = new ParallelData();
			data.Reader();

			foreach (var item in data.EvaluateAverageSalaryPerDepartment())
			{
				Console.WriteLine($"Department = '{item.Department}' and AVG Salary = {item.Salary}");
			}

			Console.WriteLine("\n");

			foreach (var item in data.FindMaxSalaryForAgeGroups())
			{
				if (item.AgeGroup == AgeGroup.Young)
					Console.WriteLine($"Age from 17 to 30 and MAX Salary = {item.Salary}");
				else if (item.AgeGroup == AgeGroup.Adult)
					Console.WriteLine($"Age from 30 to 45 and MAX Salary = {item.Salary}");
				else if (item.AgeGroup == AgeGroup.Old)
					Console.WriteLine($"Age from 46 and MAX Salary = {item.Salary}");
				else if (item.AgeGroup == AgeGroup.Other)
					Console.WriteLine($"Other age MAX Salary = {item.Salary}");
			}

			Console.WriteLine("\n\n\n");

			stopwatch.Stop();
			_listOfResult.Add($"Summary time elapsed for data: {stopwatch.Elapsed}");
		}
	}
}
